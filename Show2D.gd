extends Node2D

var rng
const SIZE = 4
const LIMIT = 128
const EPOCHES = 256
const VAL_LIMIT = 1

var delta_acc = 0
const speed = 0.1
var calc_idx = 1

var rules
var world_buffer = []

func _ready():
	rng = RandomNumberGenerator.new()

func init(in_rules="11001100", sust_at=4, sust_no=2, random_no=false):

	calc_idx = 1

	rules = make_rules_dict(in_rules)
	world_buffer = []

	var sust_row_locs = sust_at.split_floats(" ")
	var sust_col_locs = sust_no.split_floats(" ")

	#if typeof(sust_at) == TYPE_INT:
	#	for i in range(sust_at, EPOCHES, sust_at):
	#		sust_locs.append(i)
	#else:
	#	sust_locs = sust_at
	
	# Init world
	var row = gen_rand_row()
	world_buffer.append(row)
	for i in range(1, EPOCHES):
		var r = gen_ambien_row(0)
		if i in sust_row_locs: # Add sustrats
			r = gen_ambien_row(sust_col_locs, 1, random_no)
		world_buffer.append(r)

func _process(delta):
	if delta_acc - delta < speed:
		delta_acc += delta
		return

	if not rules: return
	if calc_idx >= world_buffer.size():
		return

	# Calculate step
	var row = world_buffer[calc_idx-1]
	var next = world_buffer[calc_idx]
	row = gen_next(row, rules, next)
	world_buffer[calc_idx] = row
	calc_idx += 1

	queue_redraw()

func gen_rand_row():
	var out = []
	for i in range(LIMIT):
		out.append(rng.randi_range(0, VAL_LIMIT))
	return out

func gen_ambien_row(sust_loc, only=null, random_no=false):
	var r = []
	r.resize(LIMIT)
	r.fill(-1)

	if random_no:
		sust_loc = []
		for l in range(random_no):
			sust_loc.append(rng.randi_range(0, LIMIT-1))

	var sust
	for i in sust_loc:
		if only == null:
			sust = rng.randi_range(0, VAL_LIMIT)
		else:
			sust = only
		r[i] = sust
	return r

func make_rules_dict(string):
	var out = {}
	out[[1,1,1]] = int(string[0])
	out[[1,1,0]] = int(string[1])
	out[[1,0,1]] = int(string[2])
	out[[1,0,0]] = int(string[3])
	out[[0,1,1]] = int(string[4])
	out[[0,1,0]] = int(string[5])
	out[[0,0,1]] = int(string[6])

	out[[0,0,0]] = int(string[7])
	return out

func gen_next(prev_row, rules, next_row=[]):
	var new_row = []
	for i in range(LIMIT):
		var next = rules[_grab_window(i, prev_row)]
		new_row.append(next)
	if next_row:
		for i in range(LIMIT):
			var sust = next_row[i]
			if sust >= 0:
				new_row[i] = sust

	return new_row

func _grab_window(idx, row):
	if idx == 0:
		return [row[-1], row[0], row[1]]
	if idx == LIMIT-1:
		return [row[LIMIT-2], row[LIMIT-1], row[0]]
	return row.slice(idx-1, idx+2)

####################################
# Drawing the world
func _draw():
	for i in range(world_buffer.size()):
		draw_row(i, world_buffer[i])

func draw_row(row_no, row_vals):
	for i in range(0, LIMIT):
		draw_cell(i, row_no, color_from_val(row_vals[i]))

func draw_cell(x, y, color):
	draw_rect(Rect2(x * SIZE, y * SIZE, SIZE, SIZE), color)

func color_from_val(val):
	if val == 0:
		return Color(rng.randf()/2,1 ,0)
	if val == 1:
		return Color(rng.randf()/2,0 ,1)
	return Color.BLACK

