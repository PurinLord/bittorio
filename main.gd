extends Node2D

func _input(event):
	if (event is InputEventMouseButton or event is InputEventScreenTouch) and \
		 event.is_pressed():
			$Menu.show()
			#$Menu.visible = not $Menu.visible

func _ready():
	$Menu/VBoxContainer/RunButton.connect("pressed", Callable(self, "run"))
	$Menu/VBoxContainer/CloseButton.connect("pressed", Callable($Menu, "hide"))

func run():
	$Menu.hide()
	var rules = $Menu/VBoxContainer/RulesText.text
	var sust_at = $Menu/VBoxContainer/SustAtText.text
	var sust_no = $Menu/VBoxContainer/SustNoText.text
	$Show2D.init(rules, sust_at, sust_no)
